// Do not change//
document.getElementById('cat').addEventListener('click', () => {
   alert('meow!');
 });

// When clicked, "More info" link should alert "Here's some info"
// instead of going to a new webpage
///<a id="more-info" href="https://www.yahoo.com">More info</a>
document.getElementById('more-info').addEventListener('click', function(event)  {
  
  event.preventDefault() ; alert('Here\'s some info!'); ;
});
 
// When the bark button is clicked', should alert "Bow wow!"
// Should *not* alert "meow"()

document.querySelector('button').addEventListener('click', () => {
    alert('bow wow !');event.stopPropagation()
});