// When a user clicks the + element, the count should increase by 1 on screen.
// When a user clicks the – element, the count should decrease by 1 on screen.
let counter = 0;
const plusEl = document.getElementById('plus');
const minEl = document.getElementById('min');
const ZeroEl = document.getElementById('zero')
plusEl.addEventListener('click', ()=> {
    counter ++;
    console.log(`count is ${counter}`);
}); 
 minEl.addEventListener('click', ()=> {
    counter --;
    console.log(`count is ${counter}`);
}); 
ZeroEl.addEventListener('click', ()=> {
    counter=0;
    console.log(`count is ${counter}`);
})