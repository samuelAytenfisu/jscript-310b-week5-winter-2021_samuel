// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
 let headerItem = document.getElementById("weather-head");
 headerItem.innerText = " February 10 Weather Forecast, Seattle " ;
// Change the styling of every element with class "sun" to set the color to "orange"
let suns = document.getElementsByClassName("sun");
 Array.from(suns).forEach(elements => {
     elements.style.color = 'orange' 
 });
// Change the class of the second <li> from to "sun" to "cloudy"
let list = document.getElementsByTagName('ul')[0];

list.querySelector(':nth-Child(2)').className ='cloudy';